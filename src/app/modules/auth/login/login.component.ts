import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { FuseConfigService } from "@fuse/services/config.service";
import { fuseAnimations } from "@fuse/animations";
import { AuthService } from "app/shared/services/index";
import { Router } from "@angular/router";

@Component({
    selector: "login",
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.scss"],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    loginFailed: any;
    isLoggedIn: any;
    errMessage: "";

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private authService: AuthService,
        private router: Router,
    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true,
                },
                toolbar: {
                    hidden: true,
                },
                footer: {
                    hidden: true,
                },
                sidepanel: {
                    hidden: true,
                },
            },
        };
    }

    /**
     * On init
     */
    ngOnInit(): void {
        this.loginForm = this._formBuilder.group({
            email: ["", [Validators.required, Validators.email]],
            password: ["", Validators.required],
        });
    }

    /**
     * Submit login form
     */

    submit_form() {
        console.log(this.loginForm);
        this.loginFailed = false;
        this.errMessage = "";
        if (this.loginForm.invalid) {
            return;
        }
        if (this.loginForm.valid) {
            this.authService.loginUser(this.loginForm.value).subscribe(
                (res) => {
                    this.isLoggedIn = this.authService.isLoggedIn();
                    if (this.isLoggedIn == true) {
                        this.router.navigate(["apps/dashboards/analytics"]);
                    }
                },
                (err) => {
                    // console.log(err);
                    this.loginFailed = true;
                    this.errMessage = err.error.message;
                }
            );
        } else {
            Object.keys(this.loginForm.controls).forEach((control) => {
                this.loginForm.controls[control].markAsTouched();
            });
        }
    }
}
