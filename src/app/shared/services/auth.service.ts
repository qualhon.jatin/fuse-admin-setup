import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "./../../../environments/environment";
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import { flatMap, map, finalize, tap, catchError } from "rxjs/operators";
import { Router, ActivatedRoute } from "@angular/router";


@Injectable({
  providedIn: 'root'
})
export class AuthService {
    apiUrl: string = environment.endPoint;

  constructor(
    private httpClient: HttpClient, 
    private router: Router
  ) { }
  
  loginUser(credential): Observable<any> {
    return this.httpClient
      .post(this.apiUrl + "login", credential, { observe: "response" })
      .pipe(
        map((response: HttpResponse<any>) => {
          localStorage.clear();
          var profile = {
              "name" :  response.body.data.name,
              "email" :  response.body.data.email,
              "role" :  response.body.data.role,
              "activeStatus" :  response.body.data.activeStatus,
          };
          localStorage.setItem("x-auth-token", response.body.token);
          localStorage.setItem("isLoggedIn", JSON.stringify(true));
          localStorage.setItem("userInfo", JSON.stringify(profile));
          return response;
        })
      );
  }
  isLoggedIn() {
    if (
      localStorage.getItem("isLoggedIn") &&
      localStorage.getItem("x-auth-token")
    ) {
      // logged in so return true
      return true;
    }
    return false;
  }
  logout() {
    return this.httpClient
      .get(this.apiUrl + "user/logout", { observe: "response" })
      .subscribe((response: HttpResponse<any>) => {
        localStorage.clear();
        this.router.navigate(["/auth/login"]);
        return response;
      });
  }
}
