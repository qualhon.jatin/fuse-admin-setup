import { Injectable } from "@angular/core";
import {
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
  HttpHandler,
  HttpEvent,
} from "@angular/common/http";

import { Observable, throwError } from "rxjs";
import { map, catchError } from "rxjs/operators";
import { environment } from "../../../environments/environment";

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
  constructor() {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    // Set Auth Token in Header
    let setHeaders;
    if (localStorage.getItem("isLoggedIn")) {
      setHeaders = {
        Authorization: "Bearer " + localStorage.getItem("x-auth-token"),
      };
    } else {
      setHeaders = {};
    }

    let apiReq = request.clone({ url: `${request.url}` });

    request = request.clone({
      headers: request.headers.set("Accept", "application/json"),
    });
    apiReq = request.clone({ url: `${request.url}`, setHeaders: setHeaders });
  
    // return next.handle(apiReq);
    return next.handle(apiReq).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
        }
        return event;
      })
    );
  }
}

// @Injectable()
// export class ApiInterceptor implements HttpInterceptor {
//   constructor() {}

//   intercept(
//     request: HttpRequest<any>,
//     next: HttpHandler
//   ): Observable<HttpEvent<any>> {
//     let setHeaders;
//     if (localStorage.getItem("isLoggedIn")) {
//       setHeaders = {
//         Authorization: "Bearer " + localStorage.getItem("x-auth-token"),
//       };
//     } else {
//       setHeaders = {};
//     }
//     let apiReq = request.clone({ url: `${request.url}` });

//     request = request.clone({
//       headers: request.headers.set("Accept", "application/json"),
//     });
//     apiReq = request.clone({ url: `${request.url}`, setHeaders: setHeaders });

//   }
// }
