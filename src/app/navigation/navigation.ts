import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id       : 'applications',
        title    : '',
        translate: 'NAV.APPLICATIONS',
        type     : 'group',
        icon     : 'apps',
        children : [
            {
                id       : 'dashboard',
                title    : 'Dashboard',
                type     : 'item',
                icon     : 'dashboard',
                url      : 'admin/dashboard'
            },
            // {
            //     id       : 'user-list',
            //     title    : 'User list',
            //     type     : 'collapsable',
            //     icon     : 'shopping_cart',
            //     children : [
            //         {
            //             id        : 'admin',
            //             title     : 'Admin',
            //             type      : 'item',
            //             url       : '#',
            //             exactMatch: true
            //         },
            //         {
            //             id        : 'managers',
            //             title     : 'Managers',
            //             type      : 'item',
            //             url       : '#',
            //             exactMatch: true
            //         },
            //         {
            //             id        : 'csr',
            //             title     : 'CSR',
            //             type      : 'item',
            //             url       : '#',
            //             exactMatch: true
            //         },
            //         {
            //             id        : 'user',
            //             title     : 'User',
            //             type      : 'item',
            //             url       : '#',
            //             exactMatch: true
            //         }
            //     ]
            // },
            // {
            //     id       : 'chat',
            //     title    : 'Chat',
            //     type     : 'collapsable',
            //     icon     : 'shopping_cart',
            //     children : [
            //         {
            //             id        : 'active-chats',
            //             title     : 'Active chats',
            //             type      : 'item',
            //             url       : '#',
            //             exactMatch: true
            //         },
            //         {
            //             id        : 'queue-list',
            //             title     : 'Queue list',
            //             type      : 'item',
            //             url       : '#',
            //             exactMatch: true
            //         }
            //     ]
            // },
            // {
            //     id       : 'department',
            //     title    : 'Department',
            //     type     : 'item',
            //     icon     : 'school',
            //     url      : '#'
            // },
            // {
            //     id       : 'blocked-ip',
            //     title    : 'Blocked IP',
            //     type     : 'item',
            //     icon     : 'email',
            //     url      : '#',
            // },
            // {
            //     id       : 'feedback',
            //     title    : 'Feedback',
            //     type     : 'item',
            //     icon     : 'email',
            //     url      : '/apps/mail',
            // },
            // {
            //     id       : 'profile',
            //     title    : 'Profile',
            //     type     : 'item',
            //     icon     : 'email',
            //     url      : '/apps/mail',
            // },
            // {
            //     id       : 'logout',
            //     title    : 'Logout',
            //     type     : 'item',
            //     icon     : 'email',
            //     url      : '/apps/mail',
            // }
        ]
    }
];
