import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './shared/guard/auth.guard';

const appRoutes: Routes = [
    {
        path: 'auth',
        loadChildren: () => import('./modules/auth/auth.module').then(m=> m.AuthModule)
    },
    {
        path: 'admin',
        loadChildren: () => import('./modules/admin/admin.module').then(m=> m.AdminModule),
        canActivate: [AuthGuard]
    },
    {
        path        : 'apps',
        loadChildren: () => import('./main/apps/apps.module').then(m => m.AppsModule),
        canActivate: [AuthGuard]
    },
    {
        path        : 'pages',
        loadChildren: () => import('./main/pages/pages.module').then(m => m.PagesModule),
        canActivate: [AuthGuard]
    },
    {
        path        : 'ui',
        loadChildren: () => import('./main/ui/ui.module').then(m => m.UIModule),
        canActivate: [AuthGuard]
    },
    {
        path        : 'documentation',
        loadChildren: () => import('./main/documentation/documentation.module').then(m => m.DocumentationModule),
        canActivate: [AuthGuard]
    },
    {
        path      : '**',
        redirectTo: 'apps/dashboards/analytics',
        canActivate: [AuthGuard]
    }
];

@NgModule({
    imports     : [
        RouterModule.forRoot(appRoutes, { relativeLinkResolution: 'legacy' }),
    ],
    exports:[
        RouterModule
    ]
})

export class AppRoutingModule {
}